public interface MailerConnector {
    void send(String emails, String message);
}
