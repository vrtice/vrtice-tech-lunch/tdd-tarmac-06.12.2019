import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


public class NotifierTest {

    // Tests sans librairie de mock (= mock "à la main") codé pendant la session

    @Test
    public void should_call_mailer() {

        MockDao dao = new MockDao("a@a.com;b@b.com");
        MockMailerConnector mailerConnector = new MockMailerConnector();
        Notifier notifier = new Notifier(dao, mailerConnector);

        notifier.notifyAllUsers("coucou");

        assertThat(mailerConnector.send_nbCalled).isEqualTo(1);
        assertThat(mailerConnector.send_params).contains("a@a.com;b@b.com", "coucou");
    }

    // Exemple du même test avec Mockito

    @Test
    public void should_call_mailer_with_mockito() {

        Dao dao = mock(Dao.class);
        MailerConnector mailerConnector = mock(MailerConnector.class);
        Notifier notifier = new Notifier(dao, mailerConnector);
        when(dao.getAllEmails()).thenReturn("a@a.com;b@b.com");

        notifier.notifyAllUsers("coucou");

        verify(mailerConnector).send("a@a.com;b@b.com", "coucou");
    }

}

class MockDao implements Dao {
    private String emailsToReturn;

    public MockDao(String emailsToReturn) {
        this.emailsToReturn = emailsToReturn;
    }

    @Override
    public String getAllEmails() {
        return emailsToReturn;
    }
}

class MockMailerConnector implements MailerConnector {
    public int send_nbCalled;
    public List<String> send_params;

    @Override
    public void send(String emails, String message) {
        send_nbCalled++;
        send_params = Arrays.asList(emails, message);
    }
}

